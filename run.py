import os

from src.app import create_app

if __name__ == '__main__':
  env_name = os.environ.get('FLASK_ENV','development')
  app = create_app(env_name)
  # run app
  app.run()