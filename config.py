import os


class Development(object):
    """
    Development environment configuration
    """
    DEBUG = True
    TESTING = False
    JWT_SECRET_KEY = 'XCFtopopafq'
    DATABASE_NAME = 'fireflystudycase'
    DATABASE_USER_NAME = 'postgres'
    DATABASE_USER_PASSWORD = '05022055'
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://postgres:05022055@localhost:5432/fireflystudycase'

class Test(object):
    """
    Development environment configuration
    """
    DEBUG = False
    TESTING = True
    JWT_SECRET_KEY = os.getenv('JWT_SECRET_KEY')
    DATABASE_NAME = os.getenv('DATABASE_NAME')
    DATABASE_USER_NAME = os.getenv('DATABASE_USER_NAME')
    DATABASE_USER_PASSWORD = os.getenv('DATABASE_USER_PASSWORD')
    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL')

class Production(object):
    """
    Production environment configurations
    """
    DEBUG = False
    TESTING = False
    JWT_SECRET_KEY = os.getenv('JWT_SECRET_KEY')
    DATABASE_NAME = os.getenv('DATABASE_NAME')
    DATABASE_USER_NAME = os.getenv('DATABASE_USER_NAME')
    DATABASE_USER_PASSWORD = os.getenv('DATABASE_USER_PASSWORD')
    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL')


app_config = {
    'development': Development,
    'production': Production,
    'testing' : Test
}