import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from config import app_config

app = Flask(__name__)

env_name = os.environ.get('FLASK_ENV','development')
env = app_config[env_name]

app.config['SQLALCHEMY_DATABASE_URI'] = env.SQLALCHEMY_DATABASE_URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)