import jwt
import os
import datetime
from . import custom_response
from ..Models.CompanyModel import CompanyModel
from functools import wraps
from flask import request, g
from config import app_config


class Auth():
  """
  Auth Class
  """


  @staticmethod
  def generate_token(company_id):
    """
    Generate Token Method
    """

    env_name = os.environ.get('FLASK_ENV', 'development')
    env = app_config[env_name]
    jwt_Key = env.JWT_SECRET_KEY

    try:
      payload = {
        'exp': datetime.datetime.utcnow() + datetime.timedelta(days=1),
        'iat': datetime.datetime.utcnow(),
        'sub': company_id
      }
      return jwt.encode(
        payload,
        jwt_Key,
        'HS256'
      ).decode("utf-8")
    except Exception as e:
      return custom_response("{'error': 'error in generating company token'}",400)

  @staticmethod
  def decode_token(token):
    """
    Decode token method
    """

    env_name = os.environ.get('FLASK_ENV', 'development')
    env = app_config[env_name]
    jwt_Key = env.JWT_SECRET_KEY

    re = {'data': {}, 'error': {}}
    try:
      payload = jwt.decode(token, jwt_Key)
      re['data'] = {'company_id': payload['sub']}
      return re
    except jwt.ExpiredSignatureError as e1:
      re['error'] = {'message': 'token expired, please login again'}
      return re
    except jwt.InvalidTokenError:
      re['error'] = {'message': 'Invalid token, please try again with a new token'}
      return re

  @staticmethod
  def auth_required(func):
    """
    Auth decorator
    """

    @wraps(func)
    def decorated_auth(*args, **kwargs):
      if 'api-token' not in request.headers:
        return custom_response("{'error': 'Authentication token is not available, please login to get one'}",400)
      token = request.headers.get('api-token')
      data = Auth.decode_token(token)
      if data['error']:
        return custom_response(data['error'], 400)

      company_id = data['data']['company_id']
      check_company = CompanyModel.get_company_by_id(company_id)
      if not check_company:
        return custom_response("{'error': 'company does not exist, invalid token'}", 400)
      g.company = {'id': company_id}
      return func(*args, **kwargs)

    return decorated_auth
