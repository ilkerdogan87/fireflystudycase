from flask import json, Response
from flask_bcrypt import Bcrypt
import re

bcrypt = Bcrypt()


def generate_hash(value):
    return bcrypt.generate_password_hash(value, rounds=10).decode("utf-8")


def check_hash(hashed_value, value):
    return bcrypt.check_password_hash(hashed_value, value)


def custom_response(res, status_code):

  return Response(
    mimetype="application/json",
    response=json.dumps(res),
    status=status_code
  )





# Define a function for
# for validating an Email
def check_email(email):
    # pass the regualar expression
    # and the string in search() method
    regex = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'
    if (re.search(regex, email)):
        return True

    else:
        return False