from flask import request, Blueprint, jsonify
from ..Models.CompanyModel import CompanyModel, CompanySchema
from ..Shared import custom_response
from ..Shared.Authentication import Auth
from ..Shared import check_hash,check_email

company_api = Blueprint('companies', __name__)
user_schema = CompanySchema()


@company_api.route('/signup', methods=['POST'])
def create():
    req_data = request.get_json()

    if not check_email(req_data['email']):
        message = {'error': 'Please use a valid email!'}
        return custom_response(message, 400)

    company_in_db = CompanyModel.get_company_by_email(req_data['email'])
    if company_in_db:
        message = {'error': 'User already exist, please supply another email address'}
        return custom_response(message, 400)

    user = CompanyModel(req_data)
    user.save()

    return custom_response({'success': 'Company created. Please login with your credentials'}, 200)


@company_api.route('/login', methods=['POST'])
def login():
    req_data = request.get_json()
    company = CompanyModel.get_company_by_email(req_data['email'])
    if not company:
        message = {'error': 'Company not found'}
        return custom_response(message, 400)


    if not check_hash(company.password, req_data['password']):
        message = {'error': 'Email or password incorrect!'}
        return custom_response(message, 400)

    token = Auth.generate_token(company.id)

    return jsonify({'jwt_token': token})

