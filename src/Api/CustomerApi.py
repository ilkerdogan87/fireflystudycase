from flask import request, Blueprint, g, jsonify
from ..Models.CustomerModel import CustomerModel, CustomerSchema
from ..Shared import custom_response
from ..Shared.Authentication import Auth

customer_api = Blueprint('customers', __name__)
customer_schema = CustomerSchema()


@customer_api.route('/', methods=['GET'])
@Auth.auth_required
def get_customers_for_company():
  token = request.headers.get('api-token')
  data = Auth.decode_token(token)
  company_id = data['data']['company_id']
  return jsonify({'customers': CustomerModel.get_customers_by_company_id(company_id)})


@customer_api.route('/create', methods=['POST'])
@Auth.auth_required
def create_customer():
    token = request.headers.get('api-token')
    data = Auth.decode_token(token)
    company_id = data['data']['company_id']

    req_data = request.get_json()
    req_data['company_id'] = company_id

    customer = CustomerModel(req_data)
    customer.save()
    return custom_response({'success': 'Customer created!'}, 200)
