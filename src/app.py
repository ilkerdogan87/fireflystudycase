from config import app_config
from .Api.CompanyApi import company_api as user_blueprint
from .Api.CustomerApi import customer_api as customer_blueprint
from .settings import app


def create_app(env_name):
    """
    Create app
    """

    app.config.from_object(app_config[env_name])
    app.register_blueprint(user_blueprint, url_prefix='/api/v1/companies')
    app.register_blueprint(customer_blueprint, url_prefix='/api/v1/customers')


    @app.route('/', methods=['GET'])
    def index():
        return 'Service started. Please continue /api/v1/companies!'

    return app