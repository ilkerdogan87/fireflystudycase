from ..settings import db
from ..Shared import generate_hash
from .CustomerModel import CustomerSchema
from marshmallow import fields, Schema
import datetime

class CompanyModel(db.Model):
    """
       Class constructor
    """
    __tablename__ = 'company'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120), unique=True,nullable=False)
    password = db.Column(db.String(256), nullable=False)
    created_at = db.Column(db.DateTime)
    modified_at = db.Column(db.DateTime)
    customers = db.relationship('CustomerModel', backref='company', lazy=True)

    def __init__(self, data):
        self.email = data['email']
        self.password = generate_hash(data['password'])
        self.created_at = datetime.datetime.utcnow()
        self.modified_at = datetime.datetime.utcnow()

    def json(self):
        return {'id': self.id, 'email': self.email, 'password': self.password }

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self, data):
        for key, item in data.items():
            if key == 'password':
                self.password = generate_hash(item['key'])
            setattr(self, key, item)
        self.modified_at = datetime.datetime.utcnow()
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_all_companies():
        return [CompanyModel.json(company) for company in CompanyModel.query.all()]

    @staticmethod
    def get_company_by_id(id):
        return CompanyModel.query.get(id)

    @staticmethod
    def get_company_by_email(email):
        return CompanyModel.query.filter_by(email=email).first()

    def __repr__(self):
        company_object = {
            'id':self.id,
            'email': self.email,
            'password': self.password
        }
        return company_object


class CompanySchema(Schema):
        """
        Company Schema
        """
        id = fields.Int(dump_only=True)
        email = fields.Str(required=True)
        password = fields.Str(required=True)
        created_at = fields.DateTime(dump_only=True)
        modified_at = fields.DateTime(dump_only=True)
        customers = fields.Nested(CustomerSchema, many=True)





