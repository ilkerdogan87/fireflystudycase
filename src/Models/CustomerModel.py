
from ..settings import db
from marshmallow import fields, Schema
import datetime

class CustomerModel(db.Model):
    """
       Class constructor
    """
    __tablename__ = 'customer'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), nullable=False)
    surname = db.Column(db.String(64), nullable=False)
    created_at = db.Column(db.DateTime)
    modified_at = db.Column(db.DateTime)
    company_id = db.Column(db.Integer, db.ForeignKey('company.id'), nullable=False)


    def __init__(self, data):
        self.name = data['name']
        self.surname = data['surname']
        self.company_id = data['company_id']
        self.created_at = datetime.datetime.utcnow()
        self.modified_at = datetime.datetime.utcnow()

    def json(self):
        return {'id': self.id, 'name': self.name, 'surname': self.surname, 'company_id': self.company_id}

    def save(self):
        now = datetime.datetime.utcnow()
        self.created_at = now
        self.modified_at = now
        db.session.add(self)
        db.session.commit()

    def update(self):
        self.modified_at = datetime.datetime.utcnow()
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_customers_by_company_id(company_id):
        customers = CustomerModel.query.filter_by(company_id=company_id).all()
        return [CustomerModel.json(customer) for customer in customers]

    @staticmethod
    def get_customer_by_id(id):
        return CustomerModel.query.get(id)


    def __repr__(self):
        customer_object = {
            'name': self.name,
            'surname': self.surname,
            'company_id': self.company_id
        }
        return customer_object


class CustomerSchema(Schema):
        """
        Customer Schema
        """
        id = fields.Int(dump_only=True)
        company_id = fields.Str(required=True)
        name = fields.Str(required=True)
        surname = fields.Str(required=True)
        created_at = fields.DateTime(dump_only=True)
        modified_at = fields.DateTime(dump_only=True)





