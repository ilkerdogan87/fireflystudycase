import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from config import app_config

app = Flask(__name__)
env_name = os.environ.get('FLASK_ENV','development')
env = app_config[env_name]

con = psycopg2.connect("user={} password='{}'".format(env.DATABASE_USER_NAME, env.DATABASE_USER_PASSWORD))

con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

cursor = con.cursor()


sqlCreateDatabase = "create database " + env.DATABASE_NAME + ";"

cursor.execute(sqlCreateDatabase)


app.config['SQLALCHEMY_DATABASE_URI'] = env.SQLALCHEMY_DATABASE_URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

class CompanyModel(db.Model):
    """
       Class constructor
    """
    __tablename__ = 'company'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120), unique=True,nullable=False)
    password = db.Column(db.String(256), nullable=False)
    created_at = db.Column(db.DateTime)
    modified_at = db.Column(db.DateTime)
    customers = db.relationship('CustomerModel', backref='company', lazy=True)


class CustomerModel(db.Model):
    """
       Class constructor
    """
    __tablename__ = 'customer'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), nullable=False)
    surname = db.Column(db.String(64), nullable=False)
    created_at = db.Column(db.DateTime)
    modified_at = db.Column(db.DateTime)
    company_id = db.Column(db.Integer, db.ForeignKey('company.id'), nullable=False)


#TODO  => Bu şekilde neden alamıyorum??
#from src.Models.CompanyModel import CompanyModel
#from src.Models.CustomerModel import CustomerModel

db.create_all()
db.session.commit()

