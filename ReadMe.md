

* Database olarak Postgresql kullanıldı. DockerFile mevcut ancak herhangi bir container image ayağa kaldırmadığı 
  için uygulamanın kullanılacağı makinada Postgresql kurulu olması gerekiyor
  
* Uygulama ve DockerFile dosyası ile işlem yapmadan önce src>setting.py dosyasında yer alan Development bilgileri düzenlenir

* DockerFile dosyası çalıştırılır. Böylece settings.py dosyası içinde verilen bilgiler ile Postgresql db kurulumu tamamlanır ve gerekli kütüphaneler indirilir

* python run.py komutu ile proje başlatılır


Database Detay:

company ve customer isimli 2 tablo yer almaktadır. Email ve parola ile kayıt olan bir firma, sign_up esnasında
kullandığı email ve password bilgileri ile login servisini çağırarak
token üretir. Üretilen bu token ile CustomerCreate servisi çağrılarak ilgili firma ile ilişkili bir customer kaydı oluşturulur
A firması için oluşturulan token bilgisi ile B firmasına ait customer bilgileri görüntülenemez
