FROM python:3.7

COPY requirements.txt ./
RUN pip install  -r requirements.txt

COPY /src /src
WORKDIR /src

RUN python createdb.py

EXPOSE 80